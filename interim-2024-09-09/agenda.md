# OpenPGP 2024 September Interim

- Administrivia (Stephen, dkg)

- RFC 9580 status (dkg)

- [OpenPGP Replacement Key](https://datatracker.ietf.org/doc/draft-ietf-openpgp-replacementkey/) (Andrew Gallagher)
    - Wire Formats
    - Implementation efforts
    - Usability

- AOB
