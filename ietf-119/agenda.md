# OpenPGP WG at IETF-119

Meeting: Wed 20th March, 0930-1130 (local)

Tue 19th March, 2330 - Wed 20th March 0130 (UTC)

Draft agenda:

- Administrivia (chairs)
- PQ: [draft-ietf-openpgp-pqc](https://datatracker.ietf.org/doc/draft-ietf-openpgp-pqc/):
    - KEM algs
    - KEM combiners
    - Signatures
    - Transitions/Backwards compatibility (e.g. PQ v4/v6)
- Pre-adoption discussion of other work items:
    - [draft-huigens-openpgp-persistent-symmetric-keys](https://datatracker.ietf.org/doc/draft-huigens-openpgp-persistent-symmetric-keys/)
    - [draft-gallagher-openpgp-replacementkey](https://datatracker.ietf.org/doc/draft-gallagher-openpgp-replacementkey/)
    - [draft-gallagher-openpgp-hkp](https://datatracker.ietf.org/doc/draft-gallagher-openpgp-hkp/)
    - [draft-dkg-openpgp-1pa3pc](https://datatracker.ietf.org/doc/draft-dkg-openpgp-1pa3pc/)
    - [draft-dkg-openpgp-hardware-secrets](https://datatracker.ietf.org/doc/draft-dkg-openpgp-hardware-secrets/)
    - others?
