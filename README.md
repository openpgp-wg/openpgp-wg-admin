# Administrative records for the IETF OpenPGP Working Group

The [IETF OpenPGP Working Group](https://datatracker.ietf.org/wg/openpgp) can use this space to collect information about the working group.

For example: slides for meetings, charter discussion, etc.
