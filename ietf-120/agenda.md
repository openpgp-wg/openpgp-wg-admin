# OpenPGP WG at IETF-120

Meeting: Mon 22nd July, 0930 - 1130 (local)

Mon 22nd July, 1630 - 1830 (UTC)

Draft agenda:

- Administrivia (chairs)
- Reportback from OpenPGP E-mail Summit (Patrick Brunschwig)
- PQ: [draft-ietf-openpgp-pqc](https://datatracker.ietf.org/doc/draft-ietf-openpgp-pqc/):
- Certificate Migration: [draft-gallagher-openpgp-replacementkey](https://datatracker.ietf.org/doc/draft-gallagher-openpgp-replacementkey/)
- Persistent Symmetric Keys: [draft-huigens-openpgp-persistent-symmetric-keys](https://datatracker.ietf.org/doc/draft-huigens-openpgp-persistent-symmetric-keys/)
- Salting v4 Signatures: [draft-huigens-openpgp-signature-salt-notation](https://datatracker.ietf.org/doc/draft-huigens-openpgp-signature-salt-notation/)
