# OpenPGP WG - IETF 121

## Time

- Thursday Nov 7th 2024, 0930-1130 UTC, Liffey MR 2

## Remote access:

https://meetings.conf.meetecho.com/ietf121/?session=33550

## Chairs

- Daniel Kahn Gilmor
- Stephen Farrell

## Agenda

- Administrivia
     - chairs, 5 mins
- [OpenPGP Interoperability Test Suite](https://tests.sequoia-pgp.org/) status
     - Justus Winter, 5 mins
- [Post-Quantum Cryptography in OpenPGP](https://datatracker.ietf.org/doc/draft-ietf-openpgp-pqc/)
     - Falko Strenzke, 20 mins
- [OpenPGP Key Replacement](https://datatracker.ietf.org/doc/draft-ietf-openpgp-replacementkey/)
    - Andrew Gallagher, 20 mins
- [Persistent Symmetric Keys in OpenPGP](https://datatracker.ietf.org/doc/draft-ietf-openpgp-persistent-symmetric-keys/)
    - Daniel Huigens, 20 mins
- [Stateless OpenPGP](https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli) update
    - Daniel Kahn Gillmor, 10 minutes
- AOB
