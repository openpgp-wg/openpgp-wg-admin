# OpenPGP Badges

The badges in this folder are for use by tooling and advocacy around OpenPGP.

They are made available under the [IETF Protocol Badge License Terms](https://trustee.ietf.org/wp-content/uploads/IETF-Protocol-Badge-License-Terms.pdf).
