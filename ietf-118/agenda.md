# OpenPGP @ IETF 118

- When: Thursday Session II (13:00 - 14:30 local, 12:00 - 13:30 UTC)
- Where: Berlin 3/4 (or [online via meetecho](https://meetings.conf.meetecho.com/ietf118/?group=openpgp&short=openpgp&item=1))

# Agenda

- Administrivia (chairs, 5)
    - Note well
    - Blue sheets (or equivalent)
    - Agenda bashing
- Crypto-refresh status
- Rechartering, Milestones
- Post-Quantum Cryptography
- Any Other Business
